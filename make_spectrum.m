function [Y, freq] = make_spectrum(signal, fs)
% INPUT:
% signal    :   The signal in the time domain that you want to find the spectrum of
% fs        :   The sampling frequency of the given signal

% compute spectrum (note: it will be complex-valued).
Y = fft(signal);

% The FFT needs to be scaled in order to give a physically plausible scaling.
% Y = Y * 1/(length(Y));
    % NOTE: If you do an IFFT, this scaling must NOT be done.
    % We’ll get to this in the lecture. If you are only interested
    % in the positive frequencies, you need to scale by <length(Y)/2>.

% Spectral resolution
T0 = length(signal)/fs;                 % Signal duration
df = 1/T0;                              % Frequency resolution
f_nyq = fs/2;                           % Nyquist frequency

if (mod(length(signal), 2) == 0)        % Even number of samples
    f_pos = 0:df:f_nyq;
    f_neg = -f_nyq+df:df:-df;
else                                    % Odd number of samples
    f_pos = 0:df:f_nyq;
    f_neg = -f_nyq + (f_nyq-f_pos(end)):df:-df;
end
freq = [f_pos f_neg];

    % NOTE: The first element that comes out of the FFT is the DC offset
    % (i.e., frequency 0). 
    % Each subsequent bin is spaced by the frequency resolution <df>
    % which you can calculate from the properties of the input signal. 
    % Remember the highest frequency you can get in a digitized signal....

% ...
% convert into column vector (if required)
Y = Y(:);
freq = freq(:);

% eof
end
